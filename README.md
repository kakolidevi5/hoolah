### Environment Setup
- Install Java8 [Oracle](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- Install maven [Maven](https://maven.apache.org/download.cgi) ( > 3.2 )

#### BootRun

```
mvn spring-boot:run
```

#### Or Alternatively Build and Run

```
mvn clean package
java -jar target/<artifact-name>.jar
```
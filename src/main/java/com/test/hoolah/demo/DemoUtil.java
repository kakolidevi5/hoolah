package com.test.hoolah.demo;

import java.io.BufferedReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DemoUtil {

	public static List<DemoPojo> readFromCSV(String filePath) {

		List<DemoPojo> dataList = new ArrayList<DemoPojo>();
		Path pathToFile = Paths.get(filePath);
		String line = "";

		try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {
			while ((line = br.readLine()) != null) {

				String[] attributes = line.split(";");
				DemoPojo data = createData(attributes);
				dataList.add(data);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataList;
	}

	public static DemoPojo createData(String[] attributes) {
		String id = attributes[0];
		String date = attributes[1];
		String amount = attributes[2];
		String merchant = attributes[3];
		String type = attributes[4];
		String relatedTransaction = "";
		if (attributes.length > 5) {
			relatedTransaction = attributes[5];
		}
		return new DemoPojo(id, date, amount, merchant, type, relatedTransaction);
	}

	public static void printStats(List<DemoPojo> dataList, String inputMerchant, String inputFromDate,
			String inputToDate) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			int noOfTransactions = 0;
			BigDecimal avgTransactionValue = BigDecimal.ZERO;

			Date fDate = formatter.parse(inputFromDate);
			Date tDate = formatter.parse(inputToDate);

			for (DemoPojo data : dataList) {
				Date pojoDate = formatter.parse(data.getDate());

				if (!data.getType().equalsIgnoreCase("REVERSAL") 
						&& data.getMerchant().equalsIgnoreCase(inputMerchant)
						&& pojoDate.after(fDate) && pojoDate.before(tDate)) {

					noOfTransactions++;
					avgTransactionValue = avgTransactionValue.add(new BigDecimal(data.getAmount()));
				}
			}

			System.out.println("Output : ");
			System.out.println("Number of transactions = " + noOfTransactions);
			System.out.println("Average Transaction Value = " + avgTransactionValue);

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
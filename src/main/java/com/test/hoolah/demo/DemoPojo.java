package com.test.hoolah.demo;

public class DemoPojo {

	private String id;
	private String date;
	private String amount;
	private String merchant;
	private String type;
	private String relatedTransaction;

	public DemoPojo(String id, String date, String amount, String merchant, String type, String relatedTransaction) {
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.merchant = merchant;
        this.type = type;
        this.relatedTransaction = relatedTransaction;
    }

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMerchant() {
		return merchant;
	}
	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRelatedTransaction() {
		return relatedTransaction;
	}
	public void setRelatedTransaction(String relatedTransaction) {
		this.relatedTransaction = relatedTransaction;
	}

	@Override
    public String toString() {
        return "id=" + id + ", date=" + date + ", amount=" + amount
                + ", merchant=" + merchant + ", type=" + type + ", relatedTransaction=" + relatedTransaction + "";
    }
}
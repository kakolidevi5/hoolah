package com.test.hoolah.demo;

import java.util.List;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

		try {
			Scanner scan = new Scanner(System.in);
			getInputsAndPrintOutputs(scan);
			scan.close();
		} catch (Exception e) {
		}
	}

	public static void getInputsAndPrintOutputs(Scanner scan) {
		while (true) {
			System.out.println("Enter CSV File Path, for eg. - C:\\\\Work\\\\demo\\\\file.csv");
			String filePath = scan.nextLine();

			System.out.println("Enter Merchant");
			String inputMerchant = scan.nextLine();

			System.out.println("Enter From Date");
			String inputFromDate = scan.nextLine();

			System.out.println("Enter To Date");
			String inputToDate = scan.nextLine();		

			List<DemoPojo> dataList = DemoUtil.readFromCSV(filePath);
			DemoUtil.printStats(dataList, inputMerchant, inputFromDate, inputToDate);
		}
	}
}